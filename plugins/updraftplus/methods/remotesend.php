<?php

if (!defined('UPDRAFTPLUS_DIR')) die('No direct access allowed');

/*
do_bootstrap($possible_options_array, $connect = true) # Return a WP_Error object if something goes wrong
do_upload($file) # Return true/false
do_listfiles($match)
do_delete($file) - return true/false
do_download($file, $fullpath, $start_offset) - return true/false
do_config_print()
do_config_javascript()
do_credentials_test_parameters() - return an array: keys = required _POST parameters; values = description of each
do_credentials_test($testfile) - return true/false
do_credentials_test_deletefile($testfile)
*/

// TODO: Need to deal with the issue of squillions of downloaders showing in a restore operation. Best way would be to never open the downloaders at all - make an AJAX call to see which are actually needed. (Failing that, a back-off mechanism).

if (!class_exists('UpdraftPlus_BackupModule_ViaAddon')) require_once(UPDRAFTPLUS_DIR.'/methods/viaaddon-base.php');
class UpdraftPlus_BackupModule_remotesend extends UpdraftPlus_BackupModule_ViaAddon {
	public function __construct() {
		parent::__construct('remotesend', 'Remote send', '5.2.4');
	}
}

if (!class_exists('UpdraftPlus_RemoteStorage_Addons_Base')) require_once(UPDRAFTPLUS_DIR.'/methods/addon-base.php');
class UpdraftPlus_Addons_RemoteStorage_remotesend extends UpdraftPlus_RemoteStorage_Addons_Base {

	// 2Mb. After being b64-encoded twice, this is ~ 3.7Mb = 113 seconds on 32Kb/s uplink
	private $chunk_size = 2097152;

	public function __construct() {
		# 3rd parameter: chunking? 4th: Test button?
		parent::__construct('remotesend', 'Remote send', false, false);
	}
	
	public function get_credentials() {
		return array('updraft_ssl_disableverify', 'updraft_ssl_nossl', 'updraft_ssl_useservercerts');
	}

	public function do_upload($file, $from) {

		global $updraftplus;
		$opts = $this->options;
		
		try {
			$service = $this->bootstrap();
			if (is_wp_error($service)) throw new Exception($service->get_error_message());
			if (!is_object($service)) throw new Exception("RPC service error");
		} catch (Exception $e) {
			$message = $e->getMessage().' ('.get_class($e).') (line: '.$e->getLine().', file: '.$e->getFile().')';
			$updraftplus->log("RPC service error: ".$message);
			$updraftplus->log($message, 'error');
			return false;
		}
		
		$filesize = filesize($from);
		$this->remotesend_file_size = $filesize;

		// See what the sending side currently has. This also serves as a ping. For that reason, we don't try/catch - we let them be caught at the next level up.

		$get_remote_size = $this->send_message('get_file_status', $file);
		if (is_wp_error($get_remote_size)) {
			throw new Exception($get_remote_size->get_error_message().' ('.$get_remote_size->get_error_code().')');
		}

		if (!is_array($get_remote_size) || empty($get_remote_size['response'])) throw new Exception(__('Unexpected response:','updraftplus').' '.serialize($get_remote_size));

		if ('error' == $get_remote_size['response']) {
			$msg = $get_remote_size['data'];
			// Could interpret the codes to get more interesting messages directly to the user
			throw new Exception(__('Error:','updraftplus').' '.$msg);
		}

		if (empty($get_remote_size['data']) || !isset($get_remote_size['data']['size']) || 'file_status' != $get_remote_size['response']) throw new Exception(__('Unexpected response:','updraftplus').' '.serialize($get_remote_size));

		// Possible statuses: 0=temporary file (or not present), 1=file
		if (empty($get_remote_size['data'])) {
			$remote_size = 0;
			$remote_status = 0;
		} else {
			$remote_size = (int)$get_remote_size['data']['size'];
			$remote_status = $get_remote_size['data']['status'];
		}

		$updraftplus->log("$file: existing size: ".$remote_size);
		
		// Perhaps it already exists? (if we didn't get the final confirmation)
		if ($remote_size >= $filesize && $remote_status) {
			$updraftplus->log("$file: already uploaded");
			return true;
		}

		try {
			if (false != ($handle = fopen($from, 'rb'))) {

				$this->remotesend_uploaded_size = $remote_size;

				$ret = $updraftplus->chunked_upload($this, $file, $this->method."://".trailingslashit($opts['url']).$file, $this->description, $this->chunk_size, $remote_size, true);

				fclose($handle);

				return $ret;
			} else {
				throw new Exception("Failed to open file for reading: $from");
			}
		} catch (Exception $e) {
			$updraftplus->log($this->description." upload: error: ($file) (".$e->getMessage().") (line: ".$e->getLine().', file: '.$e->getFile().')');
			return false;
		}
		
		return true;
	}

	// Return: boolean
	public function chunked_upload($file, $fp, $chunk_index, $upload_size, $upload_start, $upload_end) {

		// Already done?
		if ($upload_start < $this->remotesend_uploaded_size) return true;

		global $updraftplus;

		$service = $this->storage;
		
		$chunk = fread($fp, $upload_size);

		if (false === $chunk) {
			$updraftplus->log($this->description." upload: $file: fread failure ($upload_start)");
			return false;
		}

		$try_again = false;

		$data = array('file' => $file, 'data' => base64_encode($chunk), 'start' => $upload_start);

		if ($upload_end+1 >= $this->remotesend_file_size) {
			$data['last_chunk'] = true;
			if ('' != ($label = $updraftplus->jobdata_get('label'))) $data['label'] = $label;
		}

		// ~ 3.7Mb of data typically - timeout allows for 15.9Kb/s
		try {
			$put_chunk = $this->send_message('send_chunk', $data, 240);
		} catch (Exception $e) {
			$try_again = true;
		}

		if ($try_again || is_wp_error($put_chunk)) $put_chunk = $this->send_message('send_chunk', array('file' => $file, 'data' => $chunk, 'start' => $upload_start), 240);

		if (is_wp_error($put_chunk)) throw new Exception($put_chunk->get_error_message().' ('.$put_chunk->get_error_code().')');

		if (!is_array($put_chunk) || empty($put_chunk['response'])) throw new Exception(__('Unexpected response:','updraftplus').' '.serialize($put_chunk));

		if ('error' == $put_chunk['response']) {
			$msg = $put_chunk['data'];
			// Could interpret the codes to get more interesting messages directly to the user
			throw new Exception(__('Error:','updraftplus').' '.$msg);
		}

		if ('file_status' != $put_chunk['response']) throw new Exception(__('Unexpected response:','updraftplus').' '.serialize($put_chunk));

		// Possible statuses: 0=temporary file (or not present), 1=file
		if (empty($put_chunk['data']) || !is_array($put_chunk['data'])) {
			$updraftplus->log("Unexpected response when putting chunk $chunk_index: ".serialize($put_chunk));
			return false;
		} else {
			$remote_size = (int)$put_chunk['data']['size'];
			$remote_status = $put_chunk['data']['status'];
			$this->remotesend_uploaded_size = $remote_size;
		}

		return true;

	}

	private function send_message($message, $data = null) {
		$response = $this->storage->send_message($message, $data);
		if (is_array($response) && !empty($response['data']) && is_array($response['data']) && !empty($response['data']['php_events']) && !empty($response['data']['previous_data'])) {
			global $updraftplus;
			foreach ($response['data']['php_events'] as $logline) {
				$updraftplus->log("From remote side: ".$logline);
			}
			$response['data'] = $response['data']['previous_data'];
		}
		return $response;
	}

	public function do_bootstrap($opts, $connect = true) {
		if (!class_exists('UpdraftPlus_Remote_Communications')) require_once(UPDRAFTPLUS_DIR.'/includes/class-udrpc.php');

		$opts = $this->get_opts();

		try {
			$ud_rpc = new UpdraftPlus_Remote_Communications($opts['name_indicator']);
			$ud_rpc->set_key_local($opts['key']);
			$ud_rpc->set_destination_url($opts['url']);
			$ud_rpc->activate_replay_protection();
		} catch (Exception $e) {
			return new WP_Error('rpc_failure', "Commmunications failure: ".$e->getMessage().' (line: '.$e->getLine().', file: '.$e->getFile().')');
		}

		$this->storage = $ud_rpc;
		
		return $this->storage;
	}

	public function options_exist($opts) {
		if (is_array($opts) && !empty($opts['url']) && !empty($opts['name_indicator']) && !empty($opts['key'])) return true;
		return false;
	}

	public function get_opts() {
		global $updraftplus;
		$opts = $updraftplus->jobdata_get('remotesend_info');
		return (is_array($opts)) ? $opts : array();
	}

	// do_listfiles(), do_download(), do_delete() : the absence of any method here means that the parent will correctly throw an error
	
}

$updraftplus_addons_remotesend = new UpdraftPlus_Addons_RemoteStorage_remotesend;
