<?php

/**
 * SwapnilPatil functions and definitions
 *
 * @package SwapnilPatil
 */

/**
 * Activates default theme features
 *
 * @since 1.0
 */
function swapnilpatil_theme_setup(){

	// Make theme available for translation.
	// Translations can be filed in the /languages/ directory.
	// uncomment to enable (remove the // before load_theme_textdomain )
	// load_theme_textdomain( 'swapnilpatil', get_stylesheet_directory() . '/languages' );
	
}
add_action( 'after_setup_theme', 'swapnilpatil_theme_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function swapnilpatil_widgets_init() {
	register_sidebar( array(
		'name'			=> __( 'Sidebar', 'swapnilpatil' ),
		'id'			=> 'sidebar-1',
		'before_widget'	=> '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</aside>',
		'before_title'	=> '<h1 class="widget-title">',
		'after_title'	=> '</h1>',
	) );
}
// Uncomment the to add a new widgetized area
// add_action( 'widgets_init', 'swapnilpatil_widgets_init' );

/**
 * Register our scripts (js/css)
 *
 * @since 1.0
 */
function swapnilpatil_enqueue_scripts(){

	// Uncomment the line below and add a
	// scripts.js file to your child theme
	// to add custom javascript to your child theme
	// wp_enqueue_script( 'swapnilpatil-scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array(), '20140101', true );
	
}
// Uncomment this to add a additional scripts
//add_action( 'wp_enqueue_scripts', 'swapnilpatil_enqueue_scripts' );