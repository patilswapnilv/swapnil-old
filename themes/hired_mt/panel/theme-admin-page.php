<?php 


function hired_admin_page_styles() {  
    wp_enqueue_style( 'hired-font-awesome-admin', get_template_directory_uri() . '/fonts/font-awesome.css' ); 
	wp_enqueue_style( 'hired-style-admin', get_template_directory_uri() . '/panel/css/theme-admin-style.css' ); 
}
add_action( 'admin_enqueue_scripts', 'hired_admin_page_styles' );

     
    add_action('admin_menu', 'hired_setup_menu');  
     
    function hired_setup_menu(){
    	add_theme_page( __('Hired Theme Details', 'hired' ), __('Hired Theme Details', 'hired' ), 'manage_options', 'hired-setup', 'hired_init' ); 
    }  
     
 	function hired_init(){ 
	 	echo '<div class="grid grid-pad"><div class="col-1-1"><h1 style="text-align: center;">'; 
		printf(__('Thank you for using Hired!', 'hired' )); 
        echo "</h1></div></div>";
			
		echo '<div class="grid grid-pad" style="border-bottom: 1px solid #ccc; padding-bottom: 40px; margin-bottom: 30px;" ><div class="col-1-3"><h2>'; 
		printf(__('Premium Plugins', 'hired' )); 
        echo '</h2>';
		
		echo '<p>';
		printf(__('Want to add more functionality to your theme? Use ModernThemes Premium Plugins to add content to your widget areas and pages.', 'hired' )); 
		echo '</p>';
		
		echo '<a href="https://modernthemes.net/plugins/" target="_blank"><button>'; 
		printf(__('Get Plugins', 'hired' )); 
		echo "</button></a></div>";
		
		echo '<div class="col-1-3"><h2>'; 
		printf(__('Documentation', 'hired' ));
        echo '</h2>';  
		
		echo '<p>';
		printf(__('Check out our Hired Documentation to learn how to use Hired and for tutorials on theme functions. ', 'hired' ));  
		echo '</p>'; 
		
		echo '<a href="https://modernthemes.net/hired-documentation/" target="_blank"><button>';
		printf(__('Read Docs', 'hired' ));
		echo "</button></a></div>";
		
		echo '<div class="col-1-3"><h2>'; 
		printf(__('ModernThemes', 'hired' ));  
        echo '</h2>';  
		
		echo '<p>';
		printf(__('Need some more themes? We have a large selection of both free and premium themes to add to your collection.', 'hired' ));
		echo '</p>';
		
		echo '<a href="https://modernthemes.net/" target="_blank"><button>'; 
		printf(__('Visit Us', 'hired' ));
		echo '</button></a></div></div>';  
		
		
		echo '<div class="grid grid-pad senswp"><div class="col-1-1"><h1 style="padding-bottom: 30px; text-align: center;">';
		printf( __('Get the Premium Experience.', 'hired' )); 
		echo '</h1></div>';
		
        echo '<div class="col-1-4"><i class="fa fa-cogs"></i><h4>';
		printf( __('Plugin Compatibility', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Use our new free plugins with this theme to add functionality for things like Details Spinner, Animated Skill Bars, Past Employers, Testimonials and Projects.', 'hired' ));
		echo '</p></div>';
		
		echo '<div class="col-1-4"><i class="fa fa-home"></i><h4>';
        printf( __('About Me Page', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Instead of one small section, Hired Premium comes with an About Me page, to really show off your skills. Use our plugin compatability to add to your content.', 'hired' ));
		echo '</p></div>'; 
		
        echo '<div class="col-1-4"><i class="fa fa-image"></i><h4>';
        printf( __('Video Background', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Make your Home Page a little more modern with a video background. The best looking websites give the best first impressions.', 'hired' ));
		echo '</p></div>'; 
		
		echo '<div class="col-1-4"><i class="fa fa-th"></i><h4>';
        printf( __('Masonry Blog', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Have a blog and want it to look a little cooler? Hired Premium comes with a masonry blog layout to make your blog a little more interesting.', 'hired' ));
		echo '</p></div></div>'; 
		
		echo '<div class="grid grid-pad senswp"><div class="col-1-4"><i class="fa fa-file-image-o"></i><h4>';
		printf( __( 'Shortcodes Ultimate Extra', 'hired' ));
		echo '</h4>'; 
		
        echo '<p>';
		printf( __( 'Hired Premium comes with a Shortcodes Ultimate Extra add-on (regularly priced at $25) for <strong>free</strong>. Shortcodes Ultimate Extra features even more of your favorite shortcodes.', 'hired' ));  
		echo '</p></div>';
		
       	echo '<div class="col-1-4"><i class="fa fa-font"></i><h4>More Google Fonts</h4><p>';
		printf( __( 'Access an additional 65 Google fonts with Hired right in the WordPress customizer.', 'hired' ));
		echo '</p></div>'; 
		
       	echo '<div class="col-1-4"><i class="fa fa-file-image-o"></i><h4>';
		printf( __( 'PSD Files', 'hired' )); 
		echo '</h4>';
		
        echo '<p>';
		printf( __( 'Premium versions include PSD files. Preview your own content or showcase a customized version for your clients.', 'hired' ));
		echo '</p></div>';
            
        echo '<div class="col-1-4"><i class="fa fa-support"></i><h4>';
		printf( __( 'Free Support', 'hired' )); 
		echo '</h4>';
		
        echo '<p>';
		printf( __( 'Call on us to help you out. Premium themes come with free support that goes directly to our support staff.', 'hired' ));
		echo '</p></div></div>';
		
		echo '<div class="grid grid-pad" style="border-bottom: 1px solid #ccc; padding-bottom: 50px; margin-bottom: 30px;"><div class="col-1-1"><a href="https://modernthemes.net/premium-wordpress-themes/hired/" target="_blank"><button class="pro">'; 
		printf( __( 'View Premium Version', 'hired' )); 
		echo '</button></a></div></div>';
		
		
		
		echo '<div class="grid grid-pad senswp"><div class="col-1-1"><h1 style="padding-bottom: 30px; text-align: center;">';
		printf( __('Premium Membership. Premium Experience.', 'hired' )); 
		echo '</h1></div>';
		
        echo '<div class="col-1-4"><i class="fa fa-cogs"></i><h4>'; 
		printf( __('Plugin Compatibility', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Use our new free plugins with this theme to add functionality for things like projects, clients, team members and more. Compatible with all premium themes!', 'hired' ));
		echo '</p></div>';
		
		echo '<div class="col-1-4"><i class="fa fa-desktop"></i><h4>'; 
        printf( __('Agency Designed Themes', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('Look as good as can be with our new premium themes. Each one is agency designed with modern styles and professional layouts.', 'hired' ));
		echo '</p></div>'; 
		
        echo '<div class="col-1-4"><i class="fa fa-users"></i><h4>';
        printf( __('Membership Options', 'hired' ));
		echo '</h4>';
		
        echo '<p>';
		printf( __('We have options to fit every budget. Choose between a single theme, or access to all current and future themes for a year, or forever!', 'hired' ));
		echo '</p></div>'; 
		
		echo '<div class="col-1-4"><i class="fa fa-calendar"></i><h4>'; 
		printf( __( 'Access to New Themes', 'hired' )); 
		echo '</h4>';
		
        echo '<p>';
		printf( __( 'New themes added monthly! When you purchase a premium membership you get access to all premium themes, with new themes added monthly.', 'hired' ));   
		echo '</p></div>';
		
		
		echo '<div class="grid grid-pad" style="border-bottom: 1px solid #ccc; padding-bottom: 50px; margin-bottom: 30px;"><div class="col-1-1"><a href="https://modernthemes.net/premium-wordpress-themes/" target="_blank"><button class="pro">'; 
		printf( __( 'Get Premium Membership', 'hired' ));  
		echo '</button></a></div></div>'; 
		
		
		
		echo '<div class="grid grid-pad"><div class="col-1-1"><h2 style="text-align: center;">';   
		printf( __( 'Changelog' , 'hired' ) );
        echo "</h2>";
		
		echo '<p style="text-align: center;">'; 
		printf( __('1.0.0 - New Theme!', 'hired' ));  
		echo '</p></div></div>'; 
		
    }
?>