<?php
/**
 * @package hired
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
    	<?php the_post_thumbnail('large'); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
    	<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<p>Posted on <?php the_time('F j, Y'); ?>  in <?php the_category(', ') ?></p>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		
		<a href="<?php the_permalink(); ?>"><button>Read More</button></a> 
	</div><!-- .entry-content -->
</article><!-- #post-## -->